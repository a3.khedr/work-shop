<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Http\Response;
use PHPUnit\Framework\TestCase;
use App\Http\Controllers;
use App\library\TemperatureApiStrategyFactory;

class TemperatureTest extends TestCase
{

    #################################################
    # -> NOTES: 
    # 1- I tried testing get request to '/temperature', 
    #    Unfortunately, methods get() and call() raise some errors, canNOT identify those methods,
    #    So i tested the code inside tha API endpoint.
    #    And tested API calls through postman and attached link for documentation at postmanDocumentation.txt .  
    #################################################
 

    // testing getting temperature for successful city and date
    public function testGetTemperatureSuccessfully() {
        $city_name = "Zifta";
        $date = "2018-06-18";
        $temperatureApiStrategyFactory = new TemperatureApiStrategyFactory();
        $temperatureApiStrategy = $temperatureApiStrategyFactory->getTemperatureApiStrategy("owm");
        $temperature = $temperatureApiStrategy->findTemperature($city_name, $date);

        $this->assertContains('"cod":"200"' ,$temperature);

    }


    // testing getting temperature for city not found
    public function testGetTemperatureWrongCityName() {
        $city_name = "Ziftaaa";
        $date = date("Y/m/d");
        $temperatureApiStrategyFactory = new TemperatureApiStrategyFactory();
        $temperatureApiStrategy = $temperatureApiStrategyFactory->getTemperatureApiStrategy("owm");
        $temperature = $temperatureApiStrategy->findTemperature($city_name, $date);

        $this->assertContains('"cod":"404"' ,$temperature);
    }


    // testing getting temperature for valid city but Invalid date(invalid format, old or future dates)
    public function testGetTemperatureInvalidDate() {
        $city_name = "Zifta";
        // invalid date format
        $date = "2018-06-188";
        $temperatureApiStrategyFactory = new TemperatureApiStrategyFactory();
        $temperatureApiStrategy = $temperatureApiStrategyFactory->getTemperatureApiStrategy("owm");
        $temperature = $temperatureApiStrategy->findTemperature($city_name, $date);

        $this->assertContains('"response":[]' ,$temperature);

        // old date
        $date = "2015-06-18";
        $temperature = $temperatureApiStrategy->findTemperature($city_name, $date);

        $this->assertContains('"response":[]' ,$temperature);

        // future date more than five days
        $date = "2022-06-18";
        $temperature = $temperatureApiStrategy->findTemperature($city_name, $date);

        $this->assertContains('"response":[]' ,$temperature);

    }


}
