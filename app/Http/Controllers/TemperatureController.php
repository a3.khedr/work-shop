<?php

namespace App\Http\Controllers;
use App\library\TemperatureApiStrategyFactory;

class TemperatureController extends Controller
{
    
    public function get_temp() {
        $city_name = substr(json_encode($_GET['city']), 1, -1);
        $date = substr(json_encode($_GET['date']), 1, -1);

        $temperatureApiStrategyFactory = new TemperatureApiStrategyFactory();
        $temperatureApiStrategy = $temperatureApiStrategyFactory->getTemperatureApiStrategy("owm");
        $temperature = $temperatureApiStrategy->findTemperature($city_name, $date);

        return $temperature;
    }

    
}
