<?php

namespace App\library;

class TemperatureApiStrategyFactory {

	public function getTemperatureApiStrategy($strategy_type) {
		switch($strategy_type) {
			case "owm" :
				return new openWeatherMapApi;
			default:
				return new notValidApi;
		}
	}
}