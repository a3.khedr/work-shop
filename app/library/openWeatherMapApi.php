<?php

namespace App\library;


class openWeatherMapApi implements TemperatureApiStrategy {
	#################################################
    # -> NOTES: 
    # 1- Open Weather Map Api supports finding temperatures for only next five days for a free account.  
    # 2- There is no API end point that returns temperature of city and date on OWM Api.
    # 3- This code gets temperature for a city = $city_name for the next five days and returns empty array for invalid date.
    #################################################

	// find temperature for city = $city_name and date = $date
	// $countryCode is EG(Egyptian cities) as a default value.
	public function findTemperature($city_name, $date, $countryCode = "EG") {

		$base_uri = "http://api.openweathermap.org/data/2.5/forecast?";
		$appid = env("OWM_API_ID");
		$query = $city_name. ",".$countryCode;

		$full_url = $base_uri. "appid=". $appid. "&q=". $query;

		$string_response = @file_get_contents($full_url);

		if($string_response == false) 
			return json_encode(array(["cod" => "404", "response" => "Failed to get response from Open Weather Map Api, Most Probably City Not Found"]));

		$json_response =  json_decode($string_response, TRUE);
		if($json_response["cod"] != 200)
			return $json_response;

		return $this->parse_response($json_response, $date);
	}


	// private method to parse json response from OWM Api to get temperature on date = $date
	private function parse_response($json_response, $date) {

		$output = array();

		for( $i  = 0; $i < sizeof($json_response["list"]); $i++ ) {
			$date_txt = $json_response["list"][$i]["dt_txt"];
			if(strpos($date_txt, $date) !== false) {
				$obj = array(
			        "date_time" => $json_response["list"][$i]["dt_txt"],
			        "temp" => $json_response["list"][$i]["main"]["temp"],
			        "temp_min" => $json_response["list"][$i]["main"]["temp_min"],
			        "temp_max" => $json_response["list"][$i]["main"]["temp_max"],
			        "temp_kf" => $json_response["list"][$i]["main"]["temp_kf"],
			    );
				array_push($output, $obj);
			}
		}
		$res = array(["cod" => "200", "response" => $output]);
		return json_encode($res);
	}
}