<?php

namespace App\library;

interface TemperatureApiStrategy {
	public function findTemperature($city_name, $date);
}