populateCities("city");
set_min_and_max_dates(5);


function set_min_and_max_dates(numberOfDaysToAdd) {
    var dtToday = new Date();
    var month = dtToday.getMonth() + 1
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
       month = '0' + month.toString();
    if(day < 10)
       day = '0' + day.toString();

    var minDate = year + '-' + month + '-' + day;

    dtToday = new Date();
    dtToday.setDate(dtToday.getDate() + numberOfDaysToAdd - 1); 


    month = dtToday.getMonth() + 1
    day = dtToday.getDate();
    year = dtToday.getFullYear();
    if(month < 10)
       month = '0' + month.toString();
    if(day < 10)
       day = '0' + day.toString();

    var maxDate = year + '-' + month + '-' + day;

    $('#date').attr('min', minDate);
    $('#date').attr('max', maxDate);
}


function populateCities(cityElementId) {

    var cityElement = document.getElementById(cityElementId);

    cityElement.length = 0; // Fixed by Julian Woods
    cityElement.options[0] = new Option('Select City', '');
    cityElement.selectedIndex = 0;

    cities = "Zifta|Toukh|Tamiyah|Tahta|Sumusta as Sultani|Sidi Salim|Shirbin|Shibin al Qanatir|Samannud|Samalut|Quwaysina|Qutur|Kousa|Qalyub|Minyat an Nasr|Minuf|Matay|Mashtul as Suq|Manfalut|Madinat Sittah Uktubar|Kawm Hamadah|Kafr Saqr|Kafr ash Shaykh|Kafr ad Dawwar|Juhaynah|Izbat al Burj|Itsa|Isna|Ibshaway|Halwan|Hihya|Hawsh Isa|Fuwah|Farshut|Faraskur|Faqus|Diyarb Najm|Disuq|Dishna|Dikirnis|Dayrut|Dayr Mawas|Bush|Port Said|Bilqas Qism Awwal|Basyun|Bani Suwayf|Banha|Awsim|At Tall al Kabir|Suez|Ash Shuhada|Ashmun|Al Wasitah|Luxor|Al Qusiyah|Al Qusayr|Al Qurayn|Al Qanayat|Al Qanatir al Khayriyah|Cairo|Al Minya|Al Matariyah|Al Manzilah|Al Manshah|Al Khankah|Al Jizah|Al Jamaliyah|Ismailia|Al Ibrahimiyah|Al Hawamidiyah|Al Hamul|Al Fashn|Al Bawiti|Al Balyana|Al Bajur|Al Badari|Al Ayyat|Al Arish|Akhmim|Aja|Ad Dilinjat|Abu Qurqas|Abu Kabir|Abu al Matamir|Abnub|Az Zarqa|Ain Sukhna";

    var city_arr = cities.split("|");

    for (var i = 0; i < city_arr.length; i++) {
        cityElement.options[cityElement.length] = new Option(city_arr[i], city_arr[i]);
    }
}