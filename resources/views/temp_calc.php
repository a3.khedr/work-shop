 
 <div>
        <h1>Select City and Future Date (Within Five Days) and get Temperature</h1>
        <hr/>
        <div align="center">
	        <form>
		        <br/>Select City :
		        <select name="city" id="city" required>
		        </select>
		        <br/>
		        
		        <br/>Select Date :
		        <input name = "date" id="date" type="date" min = "" max = "" required>
		        
		        <br/> <br/>
		        <input type="submit" value = "Get Temperature">
		        <input id = "reset" type="reset">

	        </form>

	        </br> </br>


	       	 <table border = "5" width = "100%">
		         <thead>
		            <tr>
		               <th colspan = "5"><H3>Results</H3></th>
		            </tr>
		            <tr>
		               <th>Time</th>
		               <th>Temp</th>
		               <th>Temp_min</th>
		               <th>Temp_max</th>
		               <th>Temp_kf</th>
		            </tr>
		         </thead>
		         
		         
		         <tbody id = "tbody">

		         </tbody>
		         
		      </table>
        <div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="/js/app.js" type="text/javascript"></script>

<!-- This script not working when put it inside '/js/app.js' file -->
<script> 
	$("#reset").on('click', function() {
		$("tbody").empty();
	});
	$(function () {

	    $('form').on('submit', function (e) {

	      e.preventDefault();

	      $.ajax({
	        type: 'get',
	        url: '/temperature',
	        data: $('form').serialize(),
	        dataType: "json",
	        success: function (output) {
	        	$("tbody").empty();
	        	// alert(output[0]["cod"]);
	        	for (var i in output[0]["response"]) {
	        		$("tbody").append('<tr><td>' + output[0]["response"][i]["date_time"] +
	        			'</td><td>' + output[0]["response"][i]["temp"] + 
	        			'</td><td>' + output[0]["response"][i]["temp_min"] +
	        			'</td><td>' + output[0]["response"][i]["temp_max"] +
	        			'</td><td>' + output[0]["response"][i]["temp_kf"] +
	        			'</td></tr>');
	        	}
	        },
	        error: function(XMLHttpRequest, textStatus, errorThrown) { 
		        alert("Error: " + errorThrown); 
		    }
	      });
	    });
	});
</script>